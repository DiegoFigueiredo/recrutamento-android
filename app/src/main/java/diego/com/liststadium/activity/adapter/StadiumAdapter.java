package diego.com.liststadium.activity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.List;

import diego.com.liststadium.R;
import diego.com.liststadium.activity.MainActivity;
import diego.com.liststadium.model.Stadium;
import diego.com.liststadium.utils.Configs;

/**
 * Created by Diegopc on 06/11/2015.
 */
public class StadiumAdapter extends RecyclerView.Adapter<StadiumAdapter.ViewHolder> {

    private List <Stadium> mDataset;
    private static Context mContext;


    public StadiumAdapter(List<Stadium> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public StadiumAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_card_view, parent, false);


     //   ViewHolder vh =
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        mContext = holder.mView.getContext();

        Stadium stadium = mDataset.get(position);

        //magic lib!
        Ion.with(holder.imageStadium).placeholder(R.drawable.carregando).load(Configs.IMAGE_URL + stadium.getImage());
        holder.nameStadium.setText(stadium.getName());
        holder.localStadium.setText(stadium.getAddress());
        holder.descriptionLeague.setText(stadium.getChampionship());
        holder.stadium_views.setText(stadium.getViews());

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public View mView;
        public ImageView imageStadium;
        public TextView nameStadium;
        public TextView localStadium;
        public TextView descriptionLeague;
        public TextView stadium_views;

        public ViewHolder(View v) {
            super(v);
            mView = v;

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) mContext).goToDetails(nameStadium.getText().toString(), mView.getContext());
                }
            });

            imageStadium = (ImageView)v.findViewById(R.id.imageStadium);
            nameStadium = (TextView)v.findViewById(R.id.nameStadium);
            localStadium = (TextView)v.findViewById(R.id.localStadium);
            descriptionLeague = (TextView)v.findViewById(R.id.descriptionLeague);
            stadium_views = (TextView)v.findViewById(R.id.views_stadium);

        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}