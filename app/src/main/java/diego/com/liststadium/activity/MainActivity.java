package diego.com.liststadium.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import java.util.List;

import diego.com.liststadium.R;
import diego.com.liststadium.api.StadiumAPI;
import diego.com.liststadium.model.*;
import diego.com.liststadium.utils.Configs;

public class MainActivity extends AppCompatActivity implements StadiumAPI.OnStadiumGotHandler {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter adapterIon;
    private StadiumPicassoAdapter picassoAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ProgressDialog dialog;
    private Toolbar toolbar;
    private AlertDialog alert;
    private List<Stadium> listStadium;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Tritone");
        toolbar.setTitleTextColor(Color.WHITE);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        if (Configs.isOnline(this)) {
            getStadiums();
        } else {
            showDialog();
        }
    }


    @Override
    public void onStadiumGot(boolean success, List<Stadium> list, int errorCode) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Jota, e agora?");
        builder.setMessage("Abrir com glide? ou abrir com picasso? eis a questão!");
        builder.setPositiveButton("Picasso", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                picassoAdapter = new StadiumPicassoAdapter();
                mRecyclerView.setAdapter(picassoAdapter);
            }
        });
        builder.setNegativeButton("Ion", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                adapterIon = new StadiumIonAdapter();
                mRecyclerView.setAdapter(adapterIon);
            }
        });
        alert = builder.create();
        alert.show();

        dialog.cancel();
    }


    public void goToDetails(String nameStadium, Context mContext){
        Intent mainIntent = new Intent(mContext ,DetailsStadium.class);
        mainIntent.putExtra("nameStadium", nameStadium);
        MainActivity.this.startActivity(mainIntent);
    }

    public void getStadiums(){
        if (Configs.isOnline(this)) {
            dialog = ProgressDialog.show(this, "", "Loading. Please wait...", true);
            dialog.show();

            StadiumAPI stadiumAPI = new StadiumAPI();
            stadiumAPI.getAllStadium(this);
        } else {
            showDialog();
        }
    }

    public void showDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Warning");
        builder.setMessage("Check your connection");
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                getStadiums();
            }
        });
        alert = builder.create();
        alert.show();
    }

//__________________________________________________________________________________________________

    public class StadiumIonAdapter extends RecyclerView.Adapter<StadiumIonAdapter.ViewHolder> {

        @Override
        public StadiumIonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_card_view, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            Stadium stadium = listStadium.get(position);

            Ion.with(holder.imageStadium).placeholder(R.drawable.carregando).load(Configs.IMAGE_URL + stadium.getImage());
            holder.nameStadium.setText(stadium.getName());
            holder.localStadium.setText(stadium.getAddress());
            holder.descriptionLeague.setText(stadium.getChampionship());
            holder.stadium_views.setText(stadium.getViews());
            holder.mView.setOnClickListener(viewClicked(stadium.getName()));
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public View mView;
            public ImageView imageStadium;
            public TextView nameStadium;
            public TextView localStadium;
            public TextView descriptionLeague;
            public TextView stadium_views;

            public ViewHolder(View v) {
                super(v);

                mView = v;
                imageStadium = (ImageView) v.findViewById(R.id.imageStadium);
                nameStadium = (TextView) v.findViewById(R.id.nameStadium);
                localStadium = (TextView) v.findViewById(R.id.localStadium);
                descriptionLeague = (TextView) v.findViewById(R.id.descriptionLeague);
                stadium_views = (TextView) v.findViewById(R.id.views_stadium);

            }
        }

        private View.OnClickListener viewClicked(final String name) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToDetails(name, getBaseContext());
                }
            };
        }

        @Override
        public int getItemCount() {
            return listStadium.size();
        }
    }


    //__________________________________________________________________________________________________

    public class StadiumPicassoAdapter extends RecyclerView.Adapter<StadiumPicassoAdapter.ViewHolder> {

        @Override
        public StadiumPicassoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_card_view, parent, false);

            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            Stadium stadium = listStadium.get(position);

//            Ion.with(holder.imageStadium).placeholder(R.drawable.carregando).load(Configs.IMAGE_URL + stadium.getImage());

            Picasso.with(getBaseContext())
                    .load(Configs.IMAGE_URL + stadium.getImage())
                    .placeholder(R.drawable.carregando)
                    .into(holder.imageStadium);

            holder.nameStadium.setText(stadium.getName());
            holder.localStadium.setText(stadium.getAddress());
            holder.descriptionLeague.setText(stadium.getChampionship());
            holder.stadium_views.setText(stadium.getViews());
            holder.mView.setOnClickListener(viewClicked(stadium.getName()));
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public View mView;
            public ImageView imageStadium;
            public TextView nameStadium;
            public TextView localStadium;
            public TextView descriptionLeague;
            public TextView stadium_views;

            public ViewHolder(View v) {
                super(v);

                mView = v;
                imageStadium = (ImageView) v.findViewById(R.id.imageStadium);
                nameStadium = (TextView) v.findViewById(R.id.nameStadium);
                localStadium = (TextView) v.findViewById(R.id.localStadium);
                descriptionLeague = (TextView) v.findViewById(R.id.descriptionLeague);
                stadium_views = (TextView) v.findViewById(R.id.views_stadium);

            }
        }

        private View.OnClickListener viewClicked(final String name) {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToDetails(name, getBaseContext());
                }
            };
        }

        @Override
        public int getItemCount() {
            return listStadium.size();
        }
    }
}