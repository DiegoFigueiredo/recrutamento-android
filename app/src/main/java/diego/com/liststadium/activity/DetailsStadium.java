package diego.com.liststadium.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import java.util.List;

import diego.com.liststadium.R;
import diego.com.liststadium.api.DetailsStadiumAPI;
import diego.com.liststadium.model.Details;
import diego.com.liststadium.utils.Configs;

/**
 * Created by Diegopc on 09/11/2015.
 */
public class DetailsStadium extends AppCompatActivity implements  DetailsStadiumAPI.OnStadiumDetailsGotHandler {

    private String nameStadium;

    private ImageView imageStadium;
    private ImageView imageFavorite;
    private TextView name;
    private TextView address;
    private TextView city_state;
    private TextView country;
    private TextView stats;
    private TextView rating;
    private View viewGray;
    ProgressDialog dialog;
    private Toolbar toolbar;
    private AlertDialog alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_details);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Tritone");
        toolbar.setTitleTextColor(Color.WHITE);

        if (Configs.isOnline(this)) {
            getDetails();
        }
        else{
           showDialog();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }


    @Override
    public void onStadiumGot(boolean success, List<Details> list, int errorCode) {


        name = (TextView) findViewById(R.id.details_name);
        imageStadium = (ImageView) findViewById(R.id.imageDetails);
        imageFavorite = (ImageView) findViewById(R.id.imageFavorite);
        address = (TextView) findViewById(R.id.details_address);
        city_state = (TextView) findViewById(R.id.details_city_state);
        country = (TextView) findViewById(R.id.details_country);
        stats = (TextView) findViewById(R.id.details_stats);
        rating = (TextView) findViewById(R.id.rating);
        viewGray = (View) findViewById(R.id.viewGray);


        for (Details details : list){
            Ion.with(imageStadium).placeholder(R.drawable.carregando).load(Configs.IMAGE_DETAILS_URL + details.getImageStadium());
            name.setText(details.getName());
            address.setText(details.getAddress());
            city_state.setText(details.getCity() + "-" + details.getState());
            country.setText(details.getCountry());
            rating.setText(details.getRating());

            if (!(details.getStats() == null)){
                stats.setText(Html.fromHtml(details.getStats()));
            }
        }
        dialog.cancel();

        //sux, but util haha \/
        imageFavorite.setBackgroundResource(R.drawable.star_icon);
        viewGray.setBackgroundColor(Color.GRAY);
    }

    public void getDetails(){

        if (Configs.isOnline(this)) {
            dialog = ProgressDialog.show(this, "", "Loading. Please wait...", true);
            dialog.show();

            Bundle extras = getIntent().getExtras();
            nameStadium = extras.getString("nameStadium");

            DetailsStadiumAPI detailsStadiumAPI = new DetailsStadiumAPI();
            detailsStadiumAPI.getStadium(this, nameStadium);
        }
        else
            showDialog();
    }


    public void showDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Warning");
        builder.setMessage("Check your connection");
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                getDetails();
            }
        });
        alert = builder.create();
        alert.show();
    }

}