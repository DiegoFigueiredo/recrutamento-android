package diego.com.liststadium.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import diego.com.liststadium.activity.DetailsStadium;
import diego.com.liststadium.model.Details;
import diego.com.liststadium.model.Stadium;
import diego.com.liststadium.utils.Configs;
import diego.com.liststadium.utils.JsonREST;

/**
 * Created by Diegopc on 06/11/2015.
 */
public class DetailsStadiumAPI implements JsonREST.JsonHandler {

    private OnStadiumDetailsGotHandler onStadiumDetailsGotHandler;

    private static final int CODE_STADIUM = 1;
    private String complementsURL = "&info=true";





    public void getStadium(OnStadiumDetailsGotHandler handler, String nameStadium) {
        this.onStadiumDetailsGotHandler = handler;

        nameStadium = formatURL(nameStadium);

        JsonREST jsonREST = new JsonREST();
        jsonREST.get(
                Configs.DETAILS_URL + nameStadium + complementsURL,
                this,
                CODE_STADIUM
        );
    }


    public void onStadiumGot(JSONObject response, boolean success, int errorCode){
        if (!success || response == null) {
            onStadiumDetailsGotHandler.onStadiumGot(false, null, errorCode);
        } else {

            List<Details> stadiums = new ArrayList<Details>();

            try {
                JSONArray detailsStadiumsJsonArray = response.getJSONArray("avfms");
                for ( int i = 0; i < detailsStadiumsJsonArray.length(); i++){

                    JSONObject detailsJson = detailsStadiumsJsonArray.getJSONObject(i);

                    Details details = new Details();

                    details.setName(detailsJson.optString("name"));
                    details.setAddress(detailsJson.optString("address"));
                    details.setCity(detailsJson.optString("city"));
                    details.setState(detailsJson.optString("state"));
                    details.setCountry(detailsJson.optString("country"));
                    details.setStats(detailsJson.optString("stats"));
                    details.setImageStadium(detailsJson.optString("newest_image"));
                    details.setRating(detailsJson.optString("average_rating"));

                    stadiums.add(details);

                }
                onStadiumDetailsGotHandler.onStadiumGot(true, stadiums, errorCode);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String formatURL(String url){

        url = url.replace(" ", "%20");
        return url;
    }


    @Override
    public void onSuccess(int errorCode, int httpStatusCode, JSONObject response, int code) {

        switch (code) {
            case CODE_STADIUM:
                onStadiumGot(response, true, errorCode);
                break;
            default:
                throw new Error(this.getClass().getName() + " with invalid code (" + code + ")");
        }


    }

    @Override
    public void onSuccess(int errorCode, int httpStatusCode, JSONArray response, int code) {

    }

    @Override
    public void onFailure(int errorCode, int httpStatusCode, int code) {

    }

    public interface OnStadiumDetailsGotHandler {
        void onStadiumGot(boolean success, List<Details> list, int errorCode);
    }

}
