package diego.com.liststadium.api;

import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import diego.com.liststadium.model.Stadium;
import diego.com.liststadium.utils.Configs;
import diego.com.liststadium.utils.JsonREST;

/**
 * Created by Diegopc on 06/11/2015.
 */
public class StadiumAPI implements JsonREST.JsonHandler {

    private static final String URL_STADIUM = "f6bcd8e8bb853890f4fb2be8ce0418fa";
    private static final int CODE_STADIUM = 1;

    private OnStadiumGotHandler onStadiumGotHandler;


    public void getAllStadium(OnStadiumGotHandler handler) {
        this.onStadiumGotHandler = handler;

        JsonREST jsonREST = new JsonREST();
        jsonREST.get(
                Configs.BASE_URL + URL_STADIUM,
                this,
                CODE_STADIUM
        );
    }


        public void onStadiumGot(JSONObject response, boolean success, int errorCode){
            if (!success || response == null) {
                onStadiumGotHandler.onStadiumGot(false, null, errorCode);
            } else {

                List<Stadium> stadiums = new ArrayList<Stadium>();

                try {
                    JSONArray stadiumsJsonArray = response.getJSONArray("avfms");
                    for ( int i = 0; i < stadiumsJsonArray.length(); i++){

                        JSONObject stadiumsJson = stadiumsJsonArray.getJSONObject(i);

                        Stadium stadium = new Stadium();

                        stadium.setName(stadiumsJson.optString("venue"));
                        stadium.setAddress(stadiumsJson.optString("home"));
                        stadium.setChampionship(stadiumsJson.optString("away"));
                        stadium.setImage(stadiumsJson.optString("image"));
                        stadium.setViews(stadiumsJson.optString("views"));

                        stadiums.add(stadium);

                    }
                    onStadiumGotHandler.onStadiumGot(true, stadiums, errorCode);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
    }

    @Override
    public void onSuccess(int errorCode, int httpStatusCode, JSONObject response, int code) {
        switch (code) {
            case CODE_STADIUM:
                onStadiumGot(response, true, errorCode);
                break;
            default:
                throw new Error(this.getClass().getName() + " with invalid code (" + code + ")");
        }

    }

    @Override
    public void onSuccess(int errorCode, int httpStatusCode, JSONArray response, int code) {

    }

    @Override
    public void onFailure(int errorCode, int httpStatusCode, int code) {

    }


    public interface OnStadiumGotHandler {
        void onStadiumGot(boolean success, List<Stadium> list, int errorCode);
    }
}
