package diego.com.liststadium.model;

/**
 * Created by Diegopc on 06/11/2015.
 */
public class Details {

    private String imageStadium;
    private String name;
    private String address;
    private String city;
    private String state;
    private String country;
    private String stats;
    private String rating;


    public String getImageStadium() {
        return imageStadium;
    }

    public void setImageStadium(String imageStadium) {
        this.imageStadium = imageStadium;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStats() {
        return stats;
    }

    public void setStats(String stats) {
        this.stats = stats;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
