package diego.com.liststadium.utils;

import android.os.AsyncTask;
import android.util.Log;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

import java.util.Map;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.params.BasicHttpParams;
import cz.msebera.android.httpclient.params.HttpConnectionParams;
import cz.msebera.android.httpclient.params.HttpParams;

/**
 * Created by Diegopc on 06/11/2015.
 */

public class JsonREST {

    public static final int EXCEPTION_NONE = 0x00;
    public static final int EXCEPTION_INTERNET = 0x02;
    public static final int EXCEPTION_JSON = 0x04;

    private static final int HTTP_GET = 1;

    private JsonHandler lastHandler;
    private Map <String, String> lastHeaders;
    private int lastHttp;
    private int lastCodeReq;
    private String lastUrl;
    private String mContentTypeGet = "";





    public void get(String url, JsonHandler handler, int codeReq) {
        AsyncHttp asyncHttp = new AsyncHttp(handler, codeReq, HTTP_GET);
        asyncHttp.execute(url);
    }


    public class AsyncHttp extends AsyncTask<String, Integer, String> {

        private int HTTP_REQUEST;
        private String url;
        private int codeReq;
        private JsonHandler handler;
        private int httpStatusCode;
        private HttpResponse response;
        private int errorCode = EXCEPTION_NONE;

        private HttpClient httpclient;


        public AsyncHttp(JsonHandler handler, int codeReq, int http) {

            this.handler = handler;
            this.codeReq = codeReq;
            this.HTTP_REQUEST = http;

        }

        @Override
        protected String doInBackground(String... uri) {

            JsonREST.this.lastCodeReq = this.codeReq;
            JsonREST.this.lastHandler = this.handler;
            JsonREST.this.lastHttp = this.HTTP_REQUEST;
            JsonREST.this.lastUrl = uri[0];

            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
            HttpConnectionParams.setSoTimeout(httpParameters, 20000);
            httpclient = new DefaultHttpClient(httpParameters);
            String responseString = "";
            this.url = uri[0];



            try {
                executeRequest();
                if(response == null) {
                    errorCode = EXCEPTION_INTERNET;
                    return null;
                }

                StatusLine statusLine = response.getStatusLine();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                out.close();
                responseString = out.toString();

                httpStatusCode = statusLine.getStatusCode();



            } catch (ConnectTimeoutException e) {
                if (e.getMessage() != null) {
                    Log.e(Configs.LOG_TAG_NETWORK, e.getMessage());
                }
                e.printStackTrace();

            } catch (SocketTimeoutException e) {
                if (e.getMessage() != null) {
                    Log.e(Configs.LOG_TAG_NETWORK, e.getMessage());
                }
                e.printStackTrace();

            } catch (IOException e) {
                if (e.getMessage() != null) {
                    Log.e(Configs.LOG_TAG_NETWORK, e.getMessage());
                }
                e.printStackTrace();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                Log.d(JsonREST.class.getName(), ""+result);

                if (errorCode != EXCEPTION_NONE)
                    handler.onFailure(errorCode, httpStatusCode, codeReq);

                if (result.startsWith("{")){
                    handler.onSuccess(EXCEPTION_NONE, httpStatusCode, new JSONObject(result), codeReq);
                }	else if (result.startsWith("[")) {
                    handler.onSuccess(EXCEPTION_NONE, httpStatusCode, new JSONArray(result), codeReq);
                } else {
                    handler.onFailure(EXCEPTION_JSON, httpStatusCode, codeReq);
                }
            } catch (JSONException e) {
                handler.onFailure(EXCEPTION_JSON, httpStatusCode, codeReq);
            }
        }

        protected void executeRequest() throws ClientProtocolException, IOException{
            switch (HTTP_REQUEST) {

                case HTTP_GET:
                    HttpGet httpGet = new HttpGet(this.url);

                    if(!mContentTypeGet.equals(""))
                        httpGet.setHeader("Content-type", mContentTypeGet + "; charset=utf-8");

                    this.response = httpclient.execute(httpGet);
                    break;
            }
        }
    }





    public interface JsonHandler {

        public void onSuccess(int errorCode, int httpStatusCode, JSONObject response, int code);
        public void onSuccess(int errorCode, int httpStatusCode, JSONArray response, int code);
        public void onFailure(int errorCode, int httpStatusCode, int code);
    }
}
