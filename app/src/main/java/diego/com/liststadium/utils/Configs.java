package diego.com.liststadium.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

/**
 * Created by Diegopc on 06/11/2015.
 */
public class Configs {

    public static String BASE_URL = "https://aviewfrommyseat.com/avf/api/featured.php?appkey=";
    public static String IMAGE_URL = "http://aviewfrommyseat.com/wallpaper/";
    public static String DETAILS_URL = "https://aviewfrommyseat.com/avf/api/venue.php?appkey=f6bcd8e8bb853890f4fb2be8ce0418fa&venue=";
    public static String IMAGE_DETAILS_URL = "http://aviewfrommyseat.com/photos/";

    public static final String LOG_TAG_NETWORK = "STADIUM_NETWORK";

    public static boolean isOnline(Activity activity) {
        boolean lblnRet = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {
                lblnRet = true;
            }
        } catch (Exception e) {
            Log.e(Configs.class.getSimpleName(), "Error testing connectivity", e);
        }
        return lblnRet;
    }
}

